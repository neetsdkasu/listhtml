﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ListHtml
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Collections.IEnumerator enumerator = args.GetEnumerator();
            var recursive = false;
            var dst = "index.html";
            var targetExts = new List<string>();
            while (enumerator.MoveNext())
            {
                switch (enumerator.Current.ToString().ToLower())
                {
                    case "/s":
                    case "-s":
                        recursive = true;
                        break;
                    case "/o":
                    case "-o":
                    case "/out":
                    case "-out":
                    case "/output":
                    case "-output":
                    case "--output":
                        if (!enumerator.MoveNext())
                        {
                            Console.Error.WriteLine("ERROR: require output file path");
                            Environment.Exit(1);
                            return;
                        }
                        dst = enumerator.Current.ToString();
                        break;
                    case "--":
                        while (enumerator.MoveNext())
                        {
                            string ext = enumerator.Current.ToString();
                            if (!ext.StartsWith("."))
                            {
                                ext = "." + ext;
                            }
                            targetExts.Add(ext);
                        }
                        break;
                    case "/h":
                    case "-h":
                    case "/help":
                    case "-help":
                    case "--help":
                        string command = Environment.GetCommandLineArgs()[0];
                        string name = Path.GetFileNameWithoutExtension(command);
                        Console.Error.WriteLine("{0}", name);
                        Console.Error.WriteLine("  list up files under current directory and make there's links into the html file");
                        Console.Error.WriteLine("USAGE:");
                        Console.Error.WriteLine("  {0} [-s] [-o <file>] [-- <ext1> [<ext2> ...]]", name);
                        Console.Error.WriteLine("    -s                       contains files of subdirectories");
                        Console.Error.WriteLine("    -o <file>                output file (default index.html)");
                        Console.Error.WriteLine("    -- <ext1> [<ext2> ...]   extension filter");
                        Console.Error.WriteLine("EXAMPLE:");
                        Console.Error.WriteLine("  {0} -s -o links.html -- html pdf txt", name);
                        return;
                    default:
                        Console.Error.WriteLine("ERROR: unknown flag: {0}", enumerator.Current);
                        Environment.Exit(1);
                        return;
                }
            }
            string curDir = Directory.GetCurrentDirectory();
            dst = Path.Combine(curDir, dst);
            var baseUri = new Uri((new Uri(curDir)).ToString() + "/");
            string[] segments = baseUri.Segments;
            string baseName = segments[segments.Length - 1];
            var searchOptoin = recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            using (var writer = new StreamWriter(dst))
            {
                writer.WriteLine("<!DOCTYPE html>");
                writer.WriteLine(@"<html lang=""ja"">");
                writer.WriteLine("<head>");
                writer.WriteLine(@"<meta charset=""utf-8"" />");
                writer.WriteLine("<title>{0}</title>", baseName);
                writer.WriteLine("</head>");
                writer.WriteLine("<body>");
                writer.WriteLine("<h1>{0}</h1>", baseName);
                writer.WriteLine("<ul>");
                foreach (string file in Directory.EnumerateFiles(curDir, "*", searchOptoin))
                {
                    if (targetExts.Count > 0)
                    {
                        if (!Path.HasExtension(file))
                        {
                            continue;
                        }
                        string ext = Path.GetExtension(file);
                        if (!targetExts.Contains(ext))
                        {
                            continue;
                        }
                    }
                    var uri = baseUri.MakeRelativeUri(new Uri(file));
                    writer.WriteLine(@"<li><a href=""{0}"">{0}</a></li>", uri);
                }
                writer.WriteLine("</ul>");
                writer.WriteLine("</body>");
                writer.WriteLine("</html>");
            }
            Console.Error.WriteLine("output: {0}", dst);
        }
    }
}
