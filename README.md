# ListHtml

カレントディレクトリにあるファイルのリンクを作る  

例えば
```
 mydirectory
   - fuga.pdf
   - hoge.html
   - piyo.txt
```
カレントディレクトリが`mydirectory`のときにListHtmlを実行すると
以下のような内容の`index.html`ファイルをカレントディレクトリに生成する
```html
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<title>mydirectory</title>
</head>
<body>
<h1>mydirectory</h1>
<ul>
<li><a href="fuga.pdf">fuga.pdf</a></li>
<li><a href="hoge.html">hoge.html</a></li>
<li><a href="index.html">index.html</a></li>
<li><a href="piyo.txt">piyo.txt</a></li>
</ul>
</body>
</html>
```
